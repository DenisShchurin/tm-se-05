package ru.shchurin.tm.repository;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.exception.*;

import java.util.*;

public class ProjectRepository {
    private Map<String, Project> projects = new HashMap<>();

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findOne(String id) {
        return projects.get(id);
    }

    public void persist(Project project) throws AlreadyExistsException {
        if (projects.containsKey(project.getId())) {
            throw new AlreadyExistsException();
        }
        projects.put(project.getId(), project);
    }

    public void merge(Project project) {
        projects.put(project.getId(), project);
    }

    public void remove(String id) {
        projects.remove(id);
    }

    public void removeAll() {
        projects.clear();
    }

    public void removeByName(String name) {
        for (Iterator<Map.Entry<String,Project>> it = projects.entrySet().iterator(); it.hasNext();) {
            Map.Entry<String,Project> e = it.next();
            if (name.equals(e.getValue().getName())) {
                it.remove();
            }
        }
    }
}
