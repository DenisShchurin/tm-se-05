package ru.shchurin.tm.repository;

import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.exception.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TaskRepository {
    private Map<String, Task> tasks = new HashMap<>();

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findOne(String id) {
        return tasks.get(id);
    }

    public void persist(Task task) throws AlreadyExistsException {
        if (tasks.containsKey(task.getId())) {
            throw new AlreadyExistsException();
        }
        tasks.put(task.getId(), task);
    }

    public void merge(Task task) {
        tasks.put(task.getId(), task);
    }

    public void remove(String id) {
        tasks.remove(id);
    }

    public void removeAll() {
        tasks.clear();
    }

    public void removeByName(String name) {
        for (Iterator<Map.Entry<String,Task>> it = tasks.entrySet().iterator(); it.hasNext();) {
            Map.Entry<String, Task> e = it.next();
            if (name.equals(e.getValue().getName())) {
                it.remove();
            }
        }
    }
}
