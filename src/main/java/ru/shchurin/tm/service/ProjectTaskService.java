package ru.shchurin.tm.service;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.repository.ProjectRepository;
import ru.shchurin.tm.repository.TaskRepository;
import ru.shchurin.tm.exception.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProjectTaskService {
    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> getTasksOfProject(String name) throws ConsoleNameException {
        if (name == null || name.isEmpty()) {
            throw new ConsoleNameException();
        }
        List<Task> projectTasks = new ArrayList<>();
        Collection<Project> projects = projectRepository.findAll();
        String projectId = "";
        for (Project project : projects) {
            if (project.getName().equals(name)) {
                projectId = project.getId();
            }
        }

        Collection<Task> allTasks = taskRepository.findAll();
        for (Task task : allTasks) {
            if (task.getProjectId().equals(projectId)) {
                projectTasks.add(task);
            }
        }
        return projectTasks;
    }

    public void removeProjectAndTasksByName(String name) throws ConsoleNameException {
        if (name == null || name.isEmpty()) {
            throw new ConsoleNameException();
        }
        Collection<Project> projects = projectRepository.findAll();
        String projectId = "";
        for (Project project : projects) {
            if (project.getName().equals(name)) {
                projectId = project.getId();
            }
        }
        Collection<Task> tasks = taskRepository.findAll();
        for (Task task : tasks) {
            if (task.getProjectId().equals(projectId)) {
                taskRepository.remove(task.getId());
            }
        }
        projectRepository.removeByName(name);
    }
}
