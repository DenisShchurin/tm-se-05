package ru.shchurin.tm.service;

import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.repository.TaskRepository;
import ru.shchurin.tm.exception.*;

import java.util.Collection;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findOne(String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        return taskRepository.findOne(id);
    }

    public void persist(Task task) throws AlreadyExistsException, ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (task == null) {
            return;
        }
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new ConsoleNameException();
        }
        if (task.getStartDate() == null) {
            throw new ConsoleStartDateException();
        }
        if (task.getEndDate() == null) {
            throw new ConsoleEndDateException();
        }
        taskRepository.persist(task);
    }

    public void merge(Task task) throws ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (task == null) {
            return;
        }
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new ConsoleNameException();
        }
        if (task.getStartDate() == null) {
            throw new ConsoleStartDateException();
        }
        if (task.getEndDate() == null) {
            throw new ConsoleEndDateException();
        }
        taskRepository.merge(task);
    }

    public void remove(String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        taskRepository.remove(id);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public void removeByName(String name) throws ConsoleNameException {
        if (name == null || name.isEmpty()) {
            throw new ConsoleNameException();
        }
        taskRepository.removeByName(name);
    }
}
