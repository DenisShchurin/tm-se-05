package ru.shchurin.tm.service;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.repository.ProjectRepository;
import ru.shchurin.tm.exception.*;
import java.util.Collection;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findOne(String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        return projectRepository.findOne(id);
    }

    public void persist(Project project) throws AlreadyExistsException, ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (project == null) {
            return;
        }
        if (project.getName() == null || project.getName().isEmpty()) {
            throw new ConsoleNameException();
        }
        if (project.getStartDate() == null) {
            throw new ConsoleStartDateException();
        }
        if (project.getEndDate() == null) {
            throw new ConsoleEndDateException();
        }
        projectRepository.persist(project);
    }

    public void merge(Project project) throws ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (project == null) {
            return;
        }
        if (project.getName() == null || project.getName().isEmpty()) {
            throw new ConsoleNameException();
        }
        if (project.getStartDate() == null) {
            throw new ConsoleStartDateException();
        }
        if (project.getEndDate() == null) {
            throw new ConsoleEndDateException();
        }
        projectRepository.merge(project);
    }

    public void remove(String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        projectRepository.remove(id);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public void removeByName(String name) throws ConsoleNameException {
        if (name == null || name.isEmpty()) {
            throw new ConsoleNameException();
        }
        projectRepository.removeByName(name);
    }
}
