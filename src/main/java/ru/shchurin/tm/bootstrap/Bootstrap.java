package ru.shchurin.tm.bootstrap;

import ru.shchurin.tm.command.*;
import ru.shchurin.tm.repository.ProjectRepository;
import ru.shchurin.tm.repository.TaskRepository;
import ru.shchurin.tm.service.ProjectService;
import ru.shchurin.tm.service.ProjectTaskService;
import ru.shchurin.tm.service.TaskService;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.exception.*;
import java.util.*;

public class Bootstrap {
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);
    private ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final Map<String, AbstractCommand> commands = new HashMap<>();

    public void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.getCommand();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    private void init() throws CommandCorruptException {
        registry(new HelpCommand());
        registry(new ExitCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectUpdateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskUpdateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveCommand());
        registry(new TasksOfProjectCommand());
    }

    public void start() throws Exception {
        try {
            init();
        } catch (CommandCorruptException e) {
            System.out.println(e.getMessage());
            return;
        }

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = ConsoleUtil.getStringFromConsole();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }
    public ProjectService getProjectService() {
        return projectService;
    }
    public TaskService getTaskService() {
        return taskService;
    }
    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }
}
