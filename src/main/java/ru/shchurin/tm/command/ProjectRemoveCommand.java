package ru.shchurin.tm.command;

import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.exception.*;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project and his tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER PROJECT NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        try {
            bootstrap.getProjectTaskService().removeProjectAndTasksByName(name);
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("[PROJECT AND HIS TASKS REMOVED]");
    }
}
