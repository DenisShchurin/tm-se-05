package ru.shchurin.tm.command;

public class ExitCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "exit the application";
    }

    @Override
    public void execute() {
        System.out.println("GOOD BYE");
    }
}
