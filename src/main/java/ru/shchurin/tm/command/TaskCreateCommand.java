package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.DateUtil;
import ru.shchurin.tm.exception.*;
import java.text.ParseException;
import java.util.Date;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER START_DATE:");
        String start = ConsoleUtil.getStringFromConsole();
        Date startDate;
        try {
            startDate = DateUtil.parseDate(start);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG START_DATE:");
            return;
        }
        System.out.println("ENTER END_DATE:");
        String end = ConsoleUtil.getStringFromConsole();
        Date endDate;
        try {
            endDate = DateUtil.parseDate(end);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG END_DATE:");
            return;
        }
        System.out.println("ENTER PROJECT_ID:");
        String projectId = ConsoleUtil.getStringFromConsole();
        try {
            bootstrap.getTaskService().persist(new Task(name, projectId, startDate, endDate));
            System.out.println("[TASK CREATED]");
        } catch (AlreadyExistsException | ConsoleNameException | ConsoleStartDateException | ConsoleEndDateException e) {
            System.out.println(e.getMessage());
        }
    }
}
