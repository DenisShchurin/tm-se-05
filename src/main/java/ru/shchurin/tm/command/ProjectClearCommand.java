package ru.shchurin.tm.command;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        bootstrap.getProjectService().removeAll();
        System.out.println("[ALL PROJECT REMOVED]");
    }
}
