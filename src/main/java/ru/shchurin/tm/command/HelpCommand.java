package ru.shchurin.tm.command;

public class HelpCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : bootstrap.getCommands()) {
            System.out.println(command.getCommand() + ": " + command.getDescription());
        }
    }
}
