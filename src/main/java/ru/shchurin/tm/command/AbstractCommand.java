package ru.shchurin.tm.command;

import ru.shchurin.tm.bootstrap.Bootstrap;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }
    public abstract String getCommand();
    public abstract String getDescription();
    public abstract void execute() throws Exception;
}
