package ru.shchurin.tm.command;

import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.exception.*;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER TASK NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        try {
            bootstrap.getTaskService().removeByName(name);
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("[TASK REMOVED]");
    }
}
