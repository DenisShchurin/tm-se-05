package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Task;

public class TaskListCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        int index = 1;
        for (Task task: bootstrap.getTaskService().findAll()) {
            System.out.println(index++ + ". " + task);
        }
    }
}
