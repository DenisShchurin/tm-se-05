package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Project;

public class ProjectListCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        int index = 1;
        for (Project project: bootstrap.getProjectService().findAll()) {
            System.out.println(index++ + ". " + project);
        }
    }
}
