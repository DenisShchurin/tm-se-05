package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.DateUtil;
import ru.shchurin.tm.exception.*;
import java.text.ParseException;
import java.util.Date;

public class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "project-update";
    }

    @Override
    public String getDescription() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT UPDATE]");
        System.out.println("ENTER NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER ID:");
        String id = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER START_DATE:");
        String start = ConsoleUtil.getStringFromConsole();
        Date startDate;
        try {
            startDate = DateUtil.parseDate(start);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG START_DATE:");
            return;
        }
        System.out.println("ENTER END_DATE:");
        String end = ConsoleUtil.getStringFromConsole();
        Date endDate;
        try {
            endDate = DateUtil.parseDate(end);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG END_DATE:");
            return;
        }
        Project project = new Project(id, name, startDate, endDate);
        try {
            bootstrap.getProjectService().merge(project);
            System.out.println("[PROJECT UPDATED]");
        } catch (ConsoleNameException | ConsoleStartDateException | ConsoleEndDateException e) {
            System.out.println(e.getMessage());
        }
    }
}
