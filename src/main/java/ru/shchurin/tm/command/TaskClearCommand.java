package ru.shchurin.tm.command;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all task.";
    }

    @Override
    public void execute() {
        bootstrap.getTaskService().removeAll();
        System.out.println("[ALL TASK REMOVED]");
    }
}
