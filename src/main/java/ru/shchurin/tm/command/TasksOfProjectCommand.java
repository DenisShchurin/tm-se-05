package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.exception.*;
import java.util.List;

public class TasksOfProjectCommand extends AbstractCommand {
    @Override
    public String getCommand() {
        return "tasks-of-project";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS OF PROJECT]");
        System.out.println("ENTER PROJECT NAME");
        String name = ConsoleUtil.getStringFromConsole();
        List<Task> tasks;
        try {
            tasks = bootstrap.getProjectTaskService().getTasksOfProject(name);
            int index = 1;
            for (Task task : tasks) {
                System.out.println(index++ + ". " + task);
            }
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
    }
}
