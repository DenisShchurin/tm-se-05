package ru.shchurin.tm;

import ru.shchurin.tm.bootstrap.Bootstrap;

public class Application {
    public static void main( String[] args ) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}
