package ru.shchurin.tm.exception;

public class CommandCorruptException extends Exception {
    public CommandCorruptException() {
        super("Description must not be null or empty");
    }
}
