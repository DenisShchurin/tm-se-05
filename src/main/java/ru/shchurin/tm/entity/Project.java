package ru.shchurin.tm.entity;

import ru.shchurin.tm.util.DateUtil;

import java.util.Date;
import java.util.UUID;

public class Project {

    private String id = UUID.randomUUID().toString();
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;

    public Project(String name, Date startDate, Date endDate) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Project(String id, String name, Date startDate, Date endDate) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Project() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "PROJECT{" +
                "ID:'" + id + '\'' +
                ", PROJECT NAME:'" + name + '\'' +
//                ", description='" + description + '\'' +
                ", START_DATE:" + DateUtil.dateFormat(startDate) +
                ", END_DATE:" + DateUtil.dateFormat(endDate) +
                '}';
    }
}
